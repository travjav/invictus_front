import { Component, OnInit, OnDestroy } from '@angular/core';
import {HomeNavigationComponent} from './navigation/home-navigation/home-navigation.component';
import {AboutUsComponent} from '../../about/about-us/about-us.component';
import {NewUserComponent} from '../../new-user/new-user.component';
import {HomepageComponent} from '../../homepage/homepage.component';
import { Router } from '@angular/router'

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {

  }

  register(): void {
    this.router.navigate(['/newUser']);
  }

  members(): void {
    this.router.navigate(['/members']);
  }

  learnMore(): void {
    this.router.navigate(['/about']);
  }
}



