import { CookieService } from 'ngx-cookie-service';
import { DashboardService } from '../dashboard/dashboard.service';
import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/throw'
import 'rxjs/add/operator/catch';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(private auth: CookieService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        console.log('intercepted request ... ');
        console.log("Intercepting HTTP request");
        const TOKEN = this.auth.get('user_cookie');
        const AUTH_TOKEN = JSON.stringify('');
        console.log("MY TOKEN" + TOKEN);
        //  Clone the request to add the new header.
        const authReq = req.clone(
            {
                headers: req.headers.set("Authorization", TOKEN),
            }
        );
        console.log("Sending request with new header now ...");
        //send the newly created request
        return next.handle(authReq)
            .catch((error, caught) => {
                console.log("Error Occurred");
                console.log(error);
                // return the error to the method that called it
                return Observable.throw(error);
            }) as any;
    }
}
