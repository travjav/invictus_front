import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class FindUserProfileService {

  constructor(
    private http: HttpClient
  ) { }


public load_content(username): Observable<any>{

  return this.http.get('http://localhost:3200/getMemberProfile', {
    params: {
      username: username
    }
  })
}
}
