import { TestBed } from '@angular/core/testing';

import { FindUserProfileService } from './find-user-profile.service';

describe('FindUserProfileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FindUserProfileService = TestBed.get(FindUserProfileService);
    expect(service).toBeTruthy();
  });
});
