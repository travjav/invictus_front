import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {FindUserProfileService} from '../find-user-profile.service';

@Component({
  selector: 'app-find-user-profile',
  templateUrl: './find-user-profile.component.html',
  styleUrls: ['./find-user-profile.component.css'],
  providers:[FindUserProfileService]
})
export class FindUserProfileComponent implements OnInit {
   private sub: any;
   private username: string;
  constructor(
    private _loadData : FindUserProfileService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.username = params['username']; // (+) converts string 'id' to a number
      console.log(this.username)
      this._loadData.load_content(this.username)
      .subscribe(data => {
        console.log(this.username)
      // do something with all the users data here
      })
  })


  }

}
