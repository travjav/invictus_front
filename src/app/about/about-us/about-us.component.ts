import { Component, OnInit } from '@angular/core';
import {HomeNavigationComponent} from '../../landingPage/landing-page/navigation/home-navigation/home-navigation.component';
@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
