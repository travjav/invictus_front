import { TestBed, inject } from '@angular/core/testing';

import { ExerciseDataService } from './exercise-data.service';

describe('ExerciseDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExerciseDataService]
    });
  });

  it('should be created', inject([ExerciseDataService], (service: ExerciseDataService) => {
    expect(service).toBeTruthy();
  }));
});
