import { TestBed, inject } from '@angular/core/testing';

import { GetMoodService } from './get-mood.service';

describe('GetMoodService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetMoodService]
    });
  });

  it('should be created', inject([GetMoodService], (service: GetMoodService) => {
    expect(service).toBeTruthy();
  }));
});
