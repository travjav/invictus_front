import { Component, OnInit } from '@angular/core';
import { NavigationBarComponent } from '../../../navigation-bar/navigation-bar.component';
import { SleepDataService } from '../sleep-data.service';
// import { Subscription } from 'rxjs/Subscription';
import { Chart } from 'chart.js';
@Component({
  selector: 'app-sleep-chart',
  templateUrl: './sleep-chart.component.html',
  styleUrls: ['./sleep-chart.component.css'],
  providers: [SleepDataService]
})
export class SleepChartComponent implements OnInit {
  chart: any;
  constructor(

    private sleepService: SleepDataService) { }
    ngOnInit() {
      this.chart = new Chart('canvas', {
        type: 'line',
        data: {
          labels: ['Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat' , 'Sun'],
          datasets: [{
            /*
                 Values that are the set goals for the user below. lighter bar colours represent their aspired goals not actual results thus far
            */
            data: [1,12,2,14,5,7,8],
            label: "Sleep",
            borderColor: "#FF5233",
            fill: false
          }
          ]
        },
        options: {
          title: {
            display: true,
            text: 'Sleep'
          }
        }
      })
    }

  thisWeek() {
    // /  this._profileService.ObtainStatusData()//See service file for comment
    // //  .subscribe(response => {
    // //    this.statusData = response;
    // //  })
  }

  lastWeek() {
    // /  this._profileService.ObtainStatusData()//See service file for comment
    // //  .subscribe(response => {
    // //    this.statusData = response;
    // //  })
  }

  thisMonth() {
    // /  this._profileService.ObtainStatusData()//See service file for comment
    // //  .subscribe(response => {
    // //    this.statusData = response;
    // //  })
  }

  threeMonths() {
    // /  this._profileService.ObtainStatusData()//See service file for comment
    // //  .subscribe(response => {
    // //    this.statusData = response;
    // //  })
  }

  sixMonths() {
    // /  this._profileService.ObtainStatusData()//See service file for comment
    // //  .subscribe(response => {
    // //    this.statusData = response;
    // //  })
  }

  thisYear() {
    // /  this._profileService.ObtainStatusData()//See service file for comment
    // //  .subscribe(response => {
    // //    this.statusData = response;
    // //  })
  }


  oneYear() {
    // /  this._profileService.ObtainStatusData()//See service file for comment
    // //  .subscribe(response => {
    // //    this.statusData = response;
    // //  })
  }


}
