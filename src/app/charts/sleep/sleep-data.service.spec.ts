import { TestBed, inject } from '@angular/core/testing';

import { SleepDataService } from './sleep-data.service';

describe('SleepDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SleepDataService]
    });
  });

  it('should be created', inject([SleepDataService], (service: SleepDataService) => {
    expect(service).toBeTruthy();
  }));
});
