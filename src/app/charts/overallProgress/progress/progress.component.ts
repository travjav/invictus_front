import { Component, OnInit } from '@angular/core';
// import { Chart, PieChart } from 'chart.js';
import * as Chart from 'chart.js';
import { ProgressService } from '../progress.service';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.css'],
  providers: [ProgressService]
})
export class ProgressComponent implements OnInit {
  chart: any;


  constructor(
    private _progress: ProgressService) { }

  ngOnInit() {
    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: ['Jan', 'Feb', 'March', 'April', 'May', 'June'],
        datasets: [{
          data: [86, 114, 106, 106, 107, 111, 133, 221, 783, 2478],
          label: "Routine",
          borderColor: "#842109",
          fill: false
        }, {
          data: [282, 350, 411, 502, 635, 809, 947, 1402, 3700, 5267],
          label: "Diet",
          borderColor: "#035907",
          fill: false
        }, {
          data: [168, 170, 178, 190, 203, 276, 408, 547, 675, 734],
          label: "Sleeping",
          borderColor: "#04146D",
          fill: false
        }, {
          data: [40, 20, 10, 16, 24, 38, 74, 167, 508, 784],
          label: "Mood",
          borderColor: "#e8c3b9",
          fill: false
        }, {
          data: [6, 3, 2, 2, 7, 26, 82, 172, 312, 433],
          label: "Community Engagment",
          borderColor: "#c45850",
          fill: false
        }, {
          /*
                                                 Values that are the set goals for the user below. lighter bar colours represent their aspired goals not actual results thus far
       
          */
          data: [86, 114, 106, 106, 900, 111, 133, 221, 783, 2478],
          label: "Routine",
          borderColor: "#FF5233",
          fill: false
        }, {
          data: [282, 350, 411, 502, 635, 2000, 947, 1402, 3700, 5267],
          label: "Diet",
          borderColor: "#46FF33",
          fill: false
        }, {
          data: [160, 170, 178, 190, 203, 400, 408, 547, 675, 734],
          label: "Sleeping",
          borderColor: "#6833FF",
          fill: false
        }
        ]
      },
      options: {
        title: {
          display: true,
          text: 'Proximity To Set Goals || My Set Goals'
        }
      }
    })
  }

  thisWeek() {
    // /  this._profileService.ObtainStatusData()//See service file for comment
    // //  .subscribe(response => {
    // //    this.statusData = response;
    // //  })
  }

  lastWeek() {
    // /  this._profileService.ObtainStatusData()//See service file for comment
    // //  .subscribe(response => {
    // //    this.statusData = response;
    // //  })
  }

  thisMonth() {
    // /  this._profileService.ObtainStatusData()//See service file for comment
    // //  .subscribe(response => {
    // //    this.statusData = response;
    // //  })
  }

  threeMonths() {
    // /  this._profileService.ObtainStatusData()//See service file for comment
    // //  .subscribe(response => {
    // //    this.statusData = response;
    // //  })
  }

  sixMonths() {
    // /  this._profileService.ObtainStatusData()//See service file for comment
    // //  .subscribe(response => {
    // //    this.statusData = response;
    // //  })
  }

  thisYear() {
    // /  this._profileService.ObtainStatusData()//See service file for comment
    // //  .subscribe(response => {
    // //    this.statusData = response;
    // //  })
  }


  oneYear() {
    // /  this._profileService.ObtainStatusData()//See service file for comment
    // //  .subscribe(response => {
    // //    this.statusData = response;
    // //  })
  }


}
