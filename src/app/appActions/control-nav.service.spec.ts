import { TestBed, inject } from '@angular/core/testing';

import { ControlNavService } from './control-nav.service';

describe('ControlNavService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ControlNavService]
    });
  });

  it('should be created', inject([ControlNavService], (service: ControlNavService) => {
    expect(service).toBeTruthy();
  }));
});
