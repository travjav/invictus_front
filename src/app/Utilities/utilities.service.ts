import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {
  constructor(
    private cookies: CookieService
  ) { }

  get_local_time(){
    let date = new Date();
    let month = date.getUTCMonth()+ 1;
    let day = date.getUTCDate();
    let year = date.getUTCFullYear();
    let hour = date.getUTCHours();
    let minutes = date.getUTCMinutes();
    let seconds = date.getUTCSeconds();
    return year + "-" + month + "-" + day +" "+ hour + ":" + minutes + ":" + seconds;
  }

  set_session_token(username,email){
    localStorage.setItem('username',username);
    localStorage.setItem('email', email);
  }

  delete_session_token(){
    this.cookies.delete('user_cookie');
    localStorage.removeItem('username');
    localStorage.removeItem('email');
    localStorage.removeItem('user')
  }

  get_user_name(){
   return localStorage.getItem('username');
  }

  get_email(){
   return localStorage.getItem('email');
  }
}
