import { BrowserModule } from '@angular/platform-browser';
import { CalendarModule } from 'angular-calendar';
import { HomepageComponent } from './homepage/homepage.component';
import {
  HttpClientModule,
  HTTP_INTERCEPTORS,
  HttpInterceptor,
  HttpRequest,
  HttpClient,
  HttpEventType
} from '@angular/common/http';
import {Angular2ImageGalleryModule} from 'angular2-image-gallery'
import { NgxSpinnerModule } from 'ngx-spinner';
import { FullCalendarModule } from 'ng-fullcalendar';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgxGalleryModule } from 'ngx-gallery';
import { CookieService } from 'ngx-cookie-service';
import {UtilitiesService} from './Utilities/utilities.service'
import { Http, Response, RequestOptions } from '@angular/http';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TokenInterceptor } from './Interceptors/dashboard.interceptor';
import { NewUserComponent } from './new-user/new-user.component';
import { ProgressComponent } from './charts/overallProgress/progress/progress.component';
// import { FindGymComponent } from './find-gym/find-gym.component';
import { ProfileComponent } from './profile/profile.component';
import { StorageComponent } from './Local-Storage/storage/storage.component';
import { SleepChartComponent } from './charts/sleep/sleep-chart/sleep-chart.component';
import { ExerciseChartComponent } from './charts/exercise/exercise-chart/exercise-chart.component';
import { ChatComponent } from './chatspace/chat/chat.component';
import { MessageAlertsDirective } from './chatspace/message-alerts.directive';
import { AboutUsComponent } from './about/about-us/about-us.component';
import { RoutineComponent } from './profile/routine/routine/routine.component';
import { DietComponent } from './profile/diet/diet/diet.component';
import { GalleryComponent } from './profile/gallery/gallery/gallery.component';
import { PlaylistTipsComponent } from './profile/playlistTips/playlist-tips/playlist-tips.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { LandingPageComponent } from './landingPage/landing-page/landing-page.component';
import { BackgroundComponent } from './background/background/background.component';
import { SocialMediaComponent } from './social/social-media/social-media.component';
import { SupplementsComponent } from './profile/supplements/supplements/supplements.component';
import { InboxComponent } from './profile/inbox/inbox/inbox.component';
import { SettingsComponent } from './profile/settings/settings/settings.component';
import { FollowingComponent } from './profile/following/following/following.component';
import { UploadContentComponent } from './profile/uploadContent/upload-content/upload-content.component';
import { HomeNavigationComponent } from './landingPage/landing-page/navigation/home-navigation/home-navigation.component';
import { UserpostComponent } from './social/userpost/userpost.component';
//import * as Chart  from 'chart.js';
import { MoodComponent } from './charts/mood/mood/mood.component';
import { InfiniteScrollModule } from "ngx-infinite-scroll"; // https://www.npmjs.com/package/ngx-infinite-scroll
import { SpinnerComponent } from './spinner/spinner/spinner.component';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner'; // https://www.npmjs.com/package/ng4-loading-spinner
import { StarRatingModule } from 'angular-star-rating';
import { StarsComponent } from './rating/stars/stars.component';
import { MyfollowersComponent } from './profile/myFollowers/myfollowers/myfollowers.component';
import { EditsupplementsComponent } from './edit-supplement-stack/editsupplements/editsupplements.component';
import { PhotoGalleryComponent } from './Gallery/Photos/photo-gallery/photo-gallery.component'; //https://github.com/BioPhoton/angular-star-rating/wiki/Installing
import { VideosComponent } from './Gallery/Videos/videos/videos.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { DragulaModule } from 'ng2-dragula';
import * as $ from 'jquery';
import * as CryptoJS from 'crypto-js';

//Angular Material Components
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule } from '@angular/material';
import { MatButtonModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatNativeDateModule } from '@angular/material';
import { FindUserProfileComponent } from './FindUser/find-user-profile/find-user-profile.component';
//----- ------------------------------

// import 'flatpickr/dist/flatpickr.css';
// import { FlatpickrModule } from 'angularx-flatpickr';
// import { CdkTableModule } from '@angular/cdk/table';

const appRoutes: Routes = [
  { path: "members", component: HomepageComponent },
  { path: "newUser", component: NewUserComponent },
  { path: "dashboard", component: DashboardComponent },
  { path: "profile", component: ProfileComponent },
  { path: "chat", component: ChatComponent },
  { path: "diet", component: DietComponent },
  { path: "routine", component: RoutineComponent },
  { path: "playlistTips", component: PlaylistTipsComponent },
  { path: "gallery", component: GalleryComponent },
  { path: "", component: LandingPageComponent }, //####>>>>>>> home page or landing page
  { path: "about", component: AboutUsComponent },
  { path: "socialFeed", component: SocialMediaComponent },
  { path: "supplements", component: SupplementsComponent },
  { path: "inbox", component: InboxComponent },
  { path: "settings", component: SettingsComponent },
  { path: "sleeping", component: SleepChartComponent },
  { path: "following", component: FollowingComponent },
  { path: "upload", component: UploadContentComponent },
  { path: "myFollowers", component: MyfollowersComponent },
  { path: "editSupplements", component: EditsupplementsComponent },
  { path: "photoGallery", component: PhotoGalleryComponent },
  { path: "videoGallery", component: VideosComponent},
  { path: "playlist", component: PlaylistTipsComponent },
  { path: "getUserProfile/:username", component: FindUserProfileComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    DashboardComponent,
    NewUserComponent,
    ProgressComponent,
    // FindGymComponent,
    ProfileComponent,
    StorageComponent,
    SleepChartComponent,
    ExerciseChartComponent,
    ChatComponent,
    MessageAlertsDirective,
    AboutUsComponent,
    RoutineComponent,
    DietComponent,
    GalleryComponent,
    PlaylistTipsComponent,
    NavigationBarComponent,
    LandingPageComponent,
    BackgroundComponent,
    SocialMediaComponent,
    SupplementsComponent,
    InboxComponent,
    SettingsComponent,
    FollowingComponent,
    UploadContentComponent,
    HomeNavigationComponent,
    UserpostComponent,
    MoodComponent,
    SpinnerComponent,
    StarsComponent,
    MyfollowersComponent,
    EditsupplementsComponent,
    PhotoGalleryComponent,
    VideosComponent,
    FindUserProfileComponent
  ],

  imports: [
    NgxSpinnerModule,
    NgxGalleryModule,
    HttpModule,
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    Angular2ImageGalleryModule,
    // CurrencyPipe,
    // Chart,
    RouterModule.forRoot(appRoutes),
    InfiniteScrollModule,
    Ng4LoadingSpinnerModule.forRoot(),
    StarRatingModule.forRoot(),
    FullCalendarModule,
    NgxSmartModalModule.forRoot(),
    DragulaModule.forRoot(),
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatCheckboxModule,
    MatButtonModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatNativeDateModule

    //==================
  ],

  providers: [
    CookieService,
    UtilitiesService,
    Http,
    HttpModule,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true

    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
