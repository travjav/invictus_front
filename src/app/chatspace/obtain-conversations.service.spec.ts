import { TestBed, inject } from '@angular/core/testing';

import { ObtainConversationsService } from './obtain-conversations.service';

describe('ObtainConversationsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ObtainConversationsService]
    });
  });

  it('should be created', inject([ObtainConversationsService], (service: ObtainConversationsService) => {
    expect(service).toBeTruthy();
  }));
});
