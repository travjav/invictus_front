import { Component, OnInit, OnDestroy } from '@angular/core';
import { DashboardService } from './dashboard.service'
import {TokenInterceptor} from '../Interceptors/dashboard.interceptor';
import { Subscription } from "rxjs/Subscription";
import {NavigationBarComponent} from '../navigation-bar/navigation-bar.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [DashboardService]
})
export class DashboardComponent implements OnInit {
  private username: string;
  private city: string;
  private country: string;
  private sex: string;
  private profilePicture: string;
  private sender: string;
  private profile_pic: string;
  private numberOfMessages: number;
  private message: string;
  private subscription: Subscription;

  constructor(

    private dashService: DashboardService
    
  ) { 

  
  }

  ngOnInit() {
    this.sender = "james";
    this.profile_pic = "https://www.hdwallpapers.in/thumbs/2014/johnny_depp_in_transcendence-t2.jpg";
    this.message = "Hi Joe how are you?, havent saw you in a long time!";
    this.numberOfMessages = 24;
    let populateDash = this.dashService.userDashboard()
    .subscribe(data => {
      let webToken = JSON.stringify(data);
      let status = JSON.parse(data.authCode);
      if (status !== 1131) {
        console.log("Server responseie")
      } else {
        console.log(data)
        alert("You're Authenticated");
      }

    });


    // let userMessages =  this.dashService.getInboxMessages()
    // .subscribe(data =>{

    // });

    // let dietData =  this.dashService.getDietData()
    // .subscribe(data => {

    // });

    // let sleepData =  this.dashService.getSleepData()
    // .subscribe(data => {

    // });

  }

  ngOnDestroy(){
    // this.subscription.unsubscribe();
  }

}
