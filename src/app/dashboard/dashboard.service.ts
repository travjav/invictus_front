import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DashboardService {
  userDashboardInfo: string;
  cookieToken: string;

  constructor(
    private http: HttpClient
  ) { }

  userDashboard(): Observable<any> { // obtain profile images etc
    let url = "Traviss-Air:3200/dashboard";
    return this.http.get(url)

  }

  getInboxMessages(): Observable<any> {
    let url = "Traviss-Air:3200/dashboard";
    return this.http.get(url);
  }

  getSleepData(): Observable<any> {
    let url = "Traviss-Air:3200/sleep";
    return this.http.get(url);
  }

  getDietData(): Observable<any> {
    let url = "Traviss-Air:3200/diet";
    return this.http.get(url);
  }

}


