import { TestBed, inject } from '@angular/core/testing';

import { LocateGymService } from './locate-gym.service';

describe('LocateGymService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocateGymService]
    });
  });

  it('should be created', inject([LocateGymService], (service: LocateGymService) => {
    expect(service).toBeTruthy();
  }));
});
