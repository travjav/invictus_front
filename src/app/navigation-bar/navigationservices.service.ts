import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class NavigationservicesService {

  constructor(
    private http: HttpClient
  ) { }




public search_for_user(username): Observable<any>{

  return this.http.get('http://localhost:3200/searchForUsers', {
    params: {
      username: username
    }
  })
}
}
