import { TestBed, inject } from '@angular/core/testing';

import { NavigationservicesService } from './navigationservices.service';

describe('NavigationservicesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NavigationservicesService]
    });
  });

  it('should be created', inject([NavigationservicesService], (service: NavigationservicesService) => {
    expect(service).toBeTruthy();
  }));
});
