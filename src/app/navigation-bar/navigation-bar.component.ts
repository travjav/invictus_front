import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavigationservicesService } from './navigationservices.service';
import { FormBuilder, FormControl, FormGroup, Validators, FormsModule, FormArray } from '@angular/forms';
import { UtilitiesService } from '../Utilities/utilities.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css'],
  providers: [NavigationservicesService, UtilitiesService]
})
export class NavigationBarComponent implements OnInit {
  selectedValue: string;
  private usersResults: any;
  private usersList: any;
  private userSearchForm: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private utilities: UtilitiesService,
    private router: Router,
    private navService: NavigationservicesService) {
  }

  ngOnInit() {

    this.usersList = []
    this.userSearchForm = new FormGroup({
      userSearch: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'

      })
    });

  }

  ngOnDestroy() {

  }

  private logout() {
    console.log('Logging User Out')
    this.utilities.delete_session_token();
    this.router.navigate(['/']);
  }

  private search_user() {
    let userSearch = this.userSearchForm.value.userSearch;
    this.navService.search_for_user(userSearch)
      .subscribe(users => {
        this.usersList.push(users)

        this.usersResults = users.map(function (users) {
          return users.username
        });
      })
  }

  focusedUser(data) {
    this.router.navigate(['/getUserProfile', data])
  }
}
