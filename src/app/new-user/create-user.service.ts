
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CreateUserService {

  constructor(
    private http: HttpClient
  ) { }

  validateUser(email, password): Observable <any> {
    const url = "http://invictusconnected.com:5432/newUser";
    return this.http.post(url, { email: email, password: password } )
  }
}

