import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { CreateUserService } from './create-user.service';
import { Router } from '@angular/router'
import { Subscription } from 'rxjs/Subscription';
import { HomeNavigationComponent } from '../landingPage/landing-page/navigation/home-navigation/home-navigation.component';
@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css'],
  providers: [CreateUserService]
})
export class NewUserComponent implements OnInit {

  private signInForm: FormGroup;
  private subscribe: Subscription;
  constructor(
    private router: Router,
    private createUserService: CreateUserService,
    private formBuilder: FormBuilder) {
    this.signInForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscribe.unsubscribe();
  }

  //TODO create flow
  formSubmit(): void {
    console.log("Siging up new user");

    let email_field = this.signInForm.value.email;
    let password_field = this.signInForm.value.password;
    console.log("-----------------------" + email_field + password_field);

    this.createUserService.validateUser(email_field, password_field)
      .subscribe(res => {
        console.log(res);
      })
    //this.router.navigate(['/dashboard']);
  }

}











