import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditsupplementsComponent } from './editsupplements.component';

describe('EditsupplementsComponent', () => {
  let component: EditsupplementsComponent;
  let fixture: ComponentFixture<EditsupplementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditsupplementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditsupplementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
