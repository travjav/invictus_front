import { TestBed, inject } from '@angular/core/testing';

import { UpdatesupplementsService } from './updatesupplements.service';

describe('UpdatesupplementsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UpdatesupplementsService]
    });
  });

  it('should be created', inject([UpdatesupplementsService], (service: UpdatesupplementsService) => {
    expect(service).toBeTruthy();
  }));
});
