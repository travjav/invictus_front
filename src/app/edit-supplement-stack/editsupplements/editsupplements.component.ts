import { Component, OnInit } from '@angular/core';
import { UpdatesupplementsService } from './updatesupplements.service';
import { FormBuilder, FormControl, FormGroup, Validators, FormsModule } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router'
import { UtilitiesService } from '../../Utilities/utilities.service'
@Component({
  selector: 'app-editsupplements',
  templateUrl: './editsupplements.component.html',
  styleUrls: ['./editsupplements.component.scss'],
  providers: [UpdatesupplementsService, UtilitiesService]
})
export class EditsupplementsComponent implements OnInit {

  private stackForm: FormGroup;
  private subscribe: Subscription;
  private formBuilder: FormBuilder;
  private supplementData: any;

  constructor(
    private updateSupplements: UpdatesupplementsService,
    private router: Router,
    private _utilities: UtilitiesService
  ) {
  }

  ngOnInit() {
    this.stackForm = new FormGroup({
      preworkout_name: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      }),
      preworkout_review: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      }),
      intraworkout_name: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      }),
      intraworkout_review: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      }),
      postworkout_name: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      }),
      postworkout_review: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      }),
      misc_name: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      }),
      misc_review: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      })
    });
  }

  updateStack() {
    let preworkoutName = this.stackForm.value.preworkout_name;
    let preworkoutReview = this.stackForm.value.preworkout_review;
    let intraworkoutName = this.stackForm.value.intraworkout_name;
    let intraworkoutReview = this.stackForm.value.intraworkout_review;
    let postworkoutName = this.stackForm.value.postworkout_name;
    let postworkoutReview = this.stackForm.value.postworkout_review;
    let miscName = this.stackForm.value.misc_name;
    let miscReview = this.stackForm.value.misc_review;
    let username = "TravisHaycock"; // get this from localstorage
    let timestamp = this._utilities.get_local_time();
    let stack = {
      "date": timestamp,
      "username": username,
      "supplements": [
        {
          "username": username, "preworkout": preworkoutName, "preworkoutReview": preworkoutReview, "intraworkout": intraworkoutName, "intraworkoutReview": intraworkoutReview,
          "postworkoutName": postworkoutName, "postworkoutReview": postworkoutReview,
          "miscname": miscName, "miscReview": miscReview,
          "upload_date": timestamp
        }
      ]
    }

    this.updateSupplements.updateStack(stack)
      .subscribe(response => {
        this.supplementData = response;
      })
    this.router.navigate(['/supplements']);
  }


}


