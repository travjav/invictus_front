
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UpdatesupplementsService {
  constructor(
    private http: HttpClient
  ) { }


  updateStack(supplementData): Observable<any> { 

    console.log(supplementData)
     return this.http.post("http://invictusconnected.com:5432/updateSupplementStack",
     {
       "stack":supplementData
     });
  }

}

