import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class PhotoService {
  constructor(
    private http: HttpClient
  ) { }

  obtainImages(): Observable <any> {
    return this.http.get('http://invictusconnected.com:5432/getUserPhotos')
    .catch( (error: any) => Observable.throw(error.json().error || 'server error') )
  }
}
