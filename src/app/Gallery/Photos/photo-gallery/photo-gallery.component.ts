import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { PhotoService } from '../photo.service'
@Component({
  selector: 'app-photo-gallery',
  templateUrl: './photo-gallery.component.html',
  styleUrls: ['./photo-gallery.component.css'],
  providers: [PhotoService]
})

export class PhotoGalleryComponent implements OnInit {
  private postObjects: any;
  private subscribe: Subscription;

  constructor(
    private photoService: PhotoService

  ) { }
  //use a filter here for just image files, no pdfs etc.
  ngOnInit() {
    this.photoService.obtainImages()
      .subscribe((data: any) => {
        this.postObjects = data;
        console.log('=========' + this.postObjects)
      })

  }
  ngOnDestroy(): void {

  }
}