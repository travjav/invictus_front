import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { VideosService } from '../videos/videos.service'
@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css'],
  providers: [VideosService]
})
export class VideosComponent implements OnInit {
  
 private postObjects: any;
  private subscribe: Subscription;
  private userGallery;
  private folderPath: any;
  private originalName: any;
  private assigned_name: any;


  private postAuthor: any;
  private postAvatar: any;
  private postTitles: any;
  private postImages: any;
  private postHeader: any;
  private postContent: any;
  private postDate: any;
  private postMedia: any;
  private commentSection: any;
  private postComments: any;
  private postLikes: any;
  private postDislikes: any;
  private postCommentDates: any;
  // assigned_name: null
  // author_comment: null
  // author_name: null
  // comments_enabled: null
  // original_name: "cup.mp4"
  // owner_id: "3"
  // profile_image: null
  // public_comments: null
  // upload_date: null
  // upload_id: "1"
  // video_link: "https://invictus-connected-storage.s3.us-east-2.amazonaws.com/073c35ed577ea9e8a51c9010cb9e7e21.mp4"
  // __proto__: Object
  constructor(
    private videoService: VideosService

  ) { }
  //use a filter here for just image files, no pdfs etc.
  ngOnInit() {
    this.videoService.obtainVideos()
    .subscribe((data: any) => {
      this.postObjects = data;
      console.log(this.postObjects)
      this.postAuthor = this.postObjects.map(function (author){
        return author.author_name;
      })

      this.postMedia = this.postObjects.map( function (media){
        return media.video_link;
      })

  
      this.postContent = this.postObjects.map(function (content){
        return content.author_comment;
      })
  
      this.postDate = this.postObjects.map( function (date){
        return new Date (date.upload_date)
      
      })
  
    

      // this.commentSection = this.postObjects.comments; // for comment sections
      // this.postComments = this.commentSection.map( function (comments){
      //   return comments.author_comment;
      // })
  
  
      this.postLikes = this.commentSection.map( function (likes){
        return likes.postLikes;
      })
  
      this.postDislikes = this.commentSection.map( function (dislikes){
        return dislikes.dislikes;
      })
  
      this.postCommentDates - this.commentSection.map( function (dates){
        return dates.commentDate;
      })
    })
  }
  ngOnDestroy(): void {

  }

}