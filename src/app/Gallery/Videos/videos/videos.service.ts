import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class VideosService {

  constructor(
    private http: HttpClient
  ) { }


  obtainVideos(): Observable <any> {
    return this.http.get('http://invictusconnected.com:5432/getUserVideos')
    .catch( (error: any) => Observable.throw(error.json().error || 'server error') )
  }
}
