import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { HomepageService } from './homepage.service';
import { Router } from '@angular/router'
import { ISubscription } from "rxjs/Subscription";
import { UtilitiesService } from '../Utilities/utilities.service'
//./navigation/home-navigation/home-navigation.component
import { NgxSpinnerService } from 'ngx-spinner';
import { HomeNavigationComponent } from '../landingPage/landing-page/navigation/home-navigation/home-navigation.component';
@Component({
  providers: [HomepageService, UtilitiesService],
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})

export class HomepageComponent implements OnInit {

  private signInForm: FormGroup;
  private subscription: ISubscription;
  constructor(
    //private authenticatedUser: Subscription,
    private spinner: NgxSpinnerService,
    private utilities: UtilitiesService,
    private router: Router,
    private homePageSerice: HomepageService,
    private formBuilder: FormBuilder) {
    this.signInForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    // this.subscription.unsubscribe();       
  }

  set_session_items(data) {
    this.utilities.set_session_token(data.sessionInfo.email, 
      data.sessionInfo.username)
  }

  //TODO put this cookie in commonality section
  formSubmit() {
    this.spinner.show();
    let email_field = this.signInForm.value.email;
    let password_field = this.signInForm.value.password;
    console.log(email_field + password_field);
    this.homePageSerice.validateUser(email_field, password_field)
      .subscribe(data => {
        let status = JSON.parse(data.authCode);
        if (status !== 1131) {
          alert("Server Did Not Send The Proper Cookie" + status)
          console.log("Server Did Not Send The Proper Cookie" + status)
        } else {
          this.set_session_items(data);
          alert("You're Authenticated");
          this.homePageSerice.setWebToken(data.token)
          this.router.navigate(['/dashboard']);
        }
      });
  }

}
