import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { StorageService } from '../Local-Storage/storage/storage.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class HomepageService {

  constructor(
    private http: HttpClient,
    private cookieService: CookieService
  ) { }

  validateUser(email, password): Observable <any>{
    let url = "http://invictusconnected.com:5432/login";
    return this.http.post(url, { email: email, password: password })
  }

  setWebToken(value): void {
    console.log(value);
     this.cookieService.set('user_cookie', value);
  }
}
