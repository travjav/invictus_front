import { TestBed, inject } from '@angular/core/testing';

import { MyfollowersService } from './myfollowers.service';

describe('MyfollowersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyfollowersService]
    });
  });

  it('should be created', inject([MyfollowersService], (service: MyfollowersService) => {
    expect(service).toBeTruthy();
  }));
});
