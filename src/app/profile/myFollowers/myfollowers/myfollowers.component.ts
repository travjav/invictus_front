import { Component, OnInit } from '@angular/core';
import {MyfollowersService} from './myfollowers.service'
@Component({
  selector: 'app-myfollowers',
  templateUrl: './myfollowers.component.html',
  styleUrls: ['./myfollowers.component.css'],
  providers: [MyfollowersService]
})
export class MyfollowersComponent implements OnInit {

  constructor(
    private followers: MyfollowersService
  ) { }

  ngOnInit(): void {
  }
 ngOnDestroy(): void {
   //Called once, before the instance is destroyed.
   //Add 'implements OnDestroy' to the class.
 }

 getFollowers(){
   alert("gettingFollowers")
 this.followers.getFollowers();
 }

 imFollowing(): void{
  alert("im following")
  this.followers.imFollowing();
 }
}
