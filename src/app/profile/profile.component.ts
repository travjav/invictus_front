import { Component, OnInit } from '@angular/core';
import { ProfileService } from './obtain.profile.service';
import { NavigationBarComponent } from '../navigation-bar/navigation-bar.component';
import { Router } from '@angular/router'
import { ProgressComponent } from '../charts/overallProgress/progress/progress.component';
import { SleepChartComponent } from '../charts/sleep/sleep-chart/sleep-chart.component';
// import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [ProfileService]
})
export class ProfileComponent implements OnInit {
  private postAuthor: any;
  private postObjects: any;
  private postAvatar: any;
  private postTitles: any;
  private postImages: any;
  private postHeader: any;
  private postContent: any;
  private postDate: any;
  private postMedia: any;
  private commentSection: any;
  private postComments: any;
  private postLikes: any;
  private postDislikes: any;
  private postCommentDates: any;
  
  private profileImage = "https://static2.cbrimages.com/wp-content/uploads/2017/10/deadpool-punisher-fight-marvel-header.jpg?q=35&w=984&h=518&fit=crop";
  private albumName = ['Windstorm', 'Bombasto', 'Magneta'];
  private albumImage = ['https://sociable.co/wp-content/uploads/2016/12/saas-capital-1000x600.jpg', 'https://sociable.co/wp-content/uploads/2016/12/saas-capital-1000x600.jpg', 'https://sociable.co/wp-content/uploads/2016/12/saas-capital-1000x600.jpg']
  private userData: string;
  private chartData: string;
  private statusData: string;

  constructor(
    private _profileService: ProfileService,
    private router: Router
    // private subscribe: Subscription
  ) { }

  ngOnInit() {
    this._profileService.obtainUserData() //See service file for comment
      .subscribe((response) => {
        console.log(response);
        this.userData = response;
        console.log(this.userData)
      });


      this._profileService.getSocialPosts()
      .subscribe((res) => {
        this.postObjects = res;
        console.log(this.postObjects)
        this.postAuthor = this.postObjects.map(function (author){
          return author.author;
        })
    
        this.postAvatar = this.postObjects.map(function (image) {
          return image.avatar;
        })
    
        this.postTitles = this.postObjects.map(function (title) {
          return title.title;
        })
    
        this.postContent = this.postObjects.map(function (content){
          return content.postContent;
        })
    
        this.postDate = this.postObjects.map( function (date){
          return new Date (date.upload_date)
        
        })
    
        this.postMedia = this.postObjects.map( function (media){
          return media.image;
        })
    
    
        this.commentSection = this.postObjects.comments; // for comment sections
        this.postComments = this.commentSection.map( function (comments){
          return comments.postComments;
        })
    
    
        this.postLikes = this.commentSection.map( function (likes){
          return likes.postLikes;
        })
    
        this.postDislikes = this.commentSection.map( function (dislikes){
          return dislikes.dislikes;
        })
    
        this.postCommentDates - this.commentSection.map( function (dates){
          return dates.commentDate;
        })
      })
    

     

    //  this._profileService.ObtainchartData()//See service file for comment
    //  .subscribe(response => {
    //    this.chartData = response;
    //  })

    //  this._profileService.ObtainStatusData()//See service file for comment
    //  .subscribe(response => {
    //    this.statusData = response;
    //  })
  }


  diet() {
    this.router.navigate(['/diet']);
  }

  routine() {
    this.router.navigate(['/routine']);
  }

  playlistTips() {
    this.router.navigate(['/playlistTips']);
  }

  gallery() {
    this.router.navigate(['/gallery']);

  }
}
