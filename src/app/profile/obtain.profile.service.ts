import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
@Injectable()
export class ProfileService {

  userData: string;

  constructor(
    private http: HttpClient
  ) { }
  /*
    Obtain the users Inbox, Routine, Diet, Supplement stack
  */
  obtainUserData(): Observable<any> { 
    let url = "http://invictusconnected.com:5432/userProfile";
     return this.http.get(url)
  }
  getSocialPosts():Observable<any>{
    console.log("gettng posts")
    return this.http.get('http://invictusconnected.com:5432/getPosts');
  }

//   ObtainchartData():Observable<any> { 
//     let url = "http://invictusconnected.com:5432/userProfile";
//     return this.http.get(url)
//   }

//   ObtainStatusData(): Observable<any> { 
//     let url = "http://invictusconnected.com:5432/userProfile";
//     return this.http.get(url)
//   }
//  // obtain the first few pics and album covers & profile image
//   ObtainMedia(): Observable<any> {
//     let url = "http://invictusconnected.com:5432/userProfile";
//     return this.http.get(url)
//   }

//   ObtainProfileImage(): Observable<any> {
//   let url = "http://invictusconnected.com:5432/userProfile";
//   return this.http.get(url)
}

