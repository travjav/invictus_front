import { Http, Response, RequestOptions, Headers, URLSearchParams } from '@angular/http';
import { HttpClient} from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable()
export class ActionsService {

    constructor(
        private http: HttpClient
      ) { }
      
    getEvents(): Observable<any> {
    return this.http.get('http://invictusconnected.com:5432/getRoutine')
      .catch( (error: any) => Observable.throw(error.json().error || 'server error') )
    }

    updateEvent(event): Observable<any> {
        return this.http.post('http://invictusconnected.com:5432/addRoutine',
        {
          "routine":event
        });
    }
};
