import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import { CalendarComponent } from 'ng-fullcalendar'; //https://www.npmjs.com/package/ng-fullcalendar
import { Options } from 'fullcalendar';
import { ActivatedRoute } from '@angular/router';
import { ActionsService } from '../actions.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormBuilder, FormControl, FormGroup, Validators, FormsModule, FormArray } from '@angular/forms';
import { Router } from '@angular/router'
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map'
import { Deserializable } from './RoutineDTO';
import { NgxSpinnerService } from 'ngx-spinner';
import { TAIL } from '@angular/core/src/render3/interfaces/view';
@Component({
  selector: 'app-routine',
  templateUrl: './routine.component.html',
  styleUrls: ['./routine.component.css'],
  providers: [ActionsService]
})

export class RoutineComponent implements OnInit {
  private personal_record:any;
  private Journal: any;
  private username: string;
  private userWorkouts: any
  private eventArray: any;
  private selectedEvent: any;
  private selectedEventTitle: any;
  private selectedEventReps: any;
  private selectedWeight: any;
  private eventObject: any
  private calDate: any;
  private localDate: any;
  private workoutID: any;
  private exerciseArray: any;
  private exerciseOBJ: any;
  private exerciseName: any;
  private bodypartName: any;
  private set_one: any;
  private set_two: any;
  private set_three: any;
  private set_four: any;
  private weight_one: any;
  private weight_two: any;
  private weight_three: any;
  private weight_four: any;
  private uploadDate: any;
  private trainingEventData: any;
  private eventForm: FormGroup;
  private trainingDate: FormGroup;
  private bodyText: string;
  calendarOptions: Options;
  toggleAction: boolean = false;
  displayEvent: any;
  private subscribe: Subscription;
  private usersSession: Array<any>;

  @ViewChild(CalendarComponent) ucCalendar: CalendarComponent;
  constructor(
    private spinner: NgxSpinnerService,
    private _fb: FormBuilder,
    protected eventService: ActionsService,
    public ngxSmartModalService: NgxSmartModalService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.username = params['username']; // (+) converts string 'id' to a number
      this.eventService.getEvents(this.username)
        .subscribe((data: any) => {
          this.trainingEventData = data;
          console.log(this.trainingEventData)

          this.uploadDate = this.trainingEventData[0].map(function (date) {
            return date.upload_date
          });

          var allEvents = [];
          $.each(this.trainingEventData[0], function (key, value) {
            var title = value.workout[0].title
            var date = new Date(value.upload_date)
            var personal_record = value.personal_record
            var id = value.workout_id
            var workout = value.workout
            var color = "yellow"
            var textColor = 'black'

            var insertEvents = {};
            insertEvents = {
              title: title,
              id: id,
              personal_record: personal_record,
              workout: workout,
              start: date,
              color: 'yellow',   // an option!
              textColor: 'black', // an option!
            }
            allEvents.push(insertEvents);
          }
          );

          this.calendarOptions = {
            editable: true,
            eventLimit: false,
            header: {
              left: 'prev,next today',
              center: 'title',
              right: 'month,agendaWeek,agendaDay,listMonth'
            },
            events: allEvents
          };
        });

        
      this.eventForm = this._fb.group({
        repsArray: this._fb.array([
          this.getUnit()
        ])
      });

      this.trainingDate = new FormGroup({
        datePicked: new FormControl('', {
          validators: Validators.required
        })
      });
    })
  }

  clickButton(model: any) {
    this.spinner.show();
    this.addEvent()
    this.displayEvent = model;
  }

  //TODO responsible for the click
  eventClick(calEvent, jsEvent, view) {
    this.selectedEvent = calEvent.event
    var workoutSession = [];
 
    // for (let [key, value] of Object.entries(calEvent.event)) {
    //  this.personal_record = value.personal_record
    //  console.log(this.personal_record)
    // }


    for (let [key, value] of Object.entries(calEvent.event.workout)) {
      var title = value.title;
      var personal_record = value.personal_record
      var repOne = value.setOne;
      var repTwo = value.setTwo;
      var repThree = value.setThree;
      var repFour = value.setFour;
      var s1w = value.setOneWeight;
      var s2w = value.setTwoWeight;
      var s3w = value.setThreeWeight;
      var s4w = value.setFourWeight;

      var insertEvents = {};
      insertEvents = {
        title: title,
        personal_record: personal_record,
        repOne: repOne,
        repTwo: repTwo,
        repThree: repThree,
        repFour: repFour,
        weightOne: s1w,
        weightTwo: s2w,
        weightThree: s3w,
        weightFour: s4w
      }
      workoutSession.push(insertEvents);
      this.Journal = workoutSession;
 
    }
    console.log('==' + JSON.stringify(this.Journal))
    // for (let key of Object.keys(calEvent.event.workout)) {  
    //   this.Journal = calEvent.event.workout[key];

    //   console.log(JSON.stringify(this.Journal))
    // }

    // $.each(calEvent.event.workout, function (key, value) {
    //   var title = value.title
    //   var personal_record = value.peresonal_record
    //   var repOne = value.setOne;
    //   var repTwo = value.setTwo;
    //   var repThree = value.setThree;
    //   var repFour = value.setFour;
    //   var s1w = value.setOneWeight;
    //   var s2w = value.setTwoWeight;
    //   var s3w = value.setThreeWeight;
    //   var s4w = value.setFourWeight;

    //   var insertEvents = {};
    //   insertEvents = {
    //     title:title,
    //     personal_record: personal_record,
    //     repOne:repOne,
    //     repTwo:repTwo,
    //     repThree:repThree,
    //     repFour:repFour,
    //     weightOne: s1w,
    //     weightTwo: s2w,
    //     weightThree: s3w,
    //     weightFour: s4w
    //   }
    //   workoutSession.push(insertEvents);
    //   this.Journal = workoutSession;
    //   console.log('=='+ JSON.stringify(this.Journal))
    // }
    // );



    // console.log(this.eventArray)
    this.ngxSmartModalService.getModal('trainingEvent').open()
  }

  updateEvent(model: any) {
    alert('updating event')
    model = {
      event: {
        id: model.event.id,
        start: model.event.start,
        end: model.event.end,
        title: model.event.title
        // other params
      },
      duration: {
        _data: model.duration._data
      }
    }
    this.displayEvent = model;
  }

  openModal(id: string) {
    // this.modalService.open(id);
  }

  closeModal(id: string) {

  }

  createEventID() {
    var array = new Uint32Array(10);
    crypto.getRandomValues(array);
    let num = Math.floor(Math.random() * 10 + 1);
    return array[3]
  }

  addEvent() {
    let eventObject = {
      id: this.createEventID(),
      title: this.eventForm.value.bodypart,
      start: this.trainingDate.value.datePicked,
      data: this.eventForm.value
    };
    // console.log(eventObject)

    // this.ucCalendar.fullCalendar('renderEvent', eventObject)
    //   // this.eventService.updateEvent(eventObject)
    //   .subscribe(status => {
    //     console.log('=' + status);
    //   })
    // return event
  }

  /**
   * Create form unit
   */
  private getUnit() {
    return this._fb.group({
      title: [''],
      setOne: [''],
      setOneWeight: [''],
      setTwo: [''],
      setTwoWeight: [''],
      setThree: [''],
      setThreeWeight: [''],
      setFour: [''],
      setFourWeight: [''],
    });
  }
  /**
   * Add new row into form
   */
  private addUnit() {
    const control = <FormArray>this.eventForm.controls['repsArray'];
    control.push(this.getUnit());
  }
  /**
   * Remove from form on click delete button
   */
  private removeUnit(i: number) {
    const control = <FormArray>this.eventForm.controls['repsArray'];
    control.removeAt(i);
  }
}
