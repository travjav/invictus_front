import { TestBed, inject } from '@angular/core/testing';

import { UpdateRoutineService } from './update-routine.service';

describe('UpdateRoutineService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UpdateRoutineService]
    });
  });

  it('should be created', inject([UpdateRoutineService], (service: UpdateRoutineService) => {
    expect(service).toBeTruthy();
  }));
});
