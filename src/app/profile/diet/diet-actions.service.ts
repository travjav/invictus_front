
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DietActionsService {

  constructor(private http: HttpClient) { }

  thisWeek(): Observable<any> {
    return this.http.get('')
  }

  lastWeek(): Observable<any> {
    return this.http.get('')
  }

  thisMonth(): Observable<any> {
    return this.http.get('')
  }

  threeMonths(): Observable<any> {
    return this.http.get('')
  }

  sixMonths(): Observable<any> {
    return this.http.get('')
  }

  thisYear(): Observable<any> {
    return this.http.get('')
  }

  oneYear(): Observable<any> {
    return this.http.get('')
  }
}