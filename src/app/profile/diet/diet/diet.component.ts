import { Component, OnInit } from '@angular/core';
import {NavigationBarComponent} from '../../../navigation-bar/navigation-bar.component';
import { Subscription } from "rxjs/Subscription";
@Component({
  selector: 'app-diet',
  templateUrl: './diet.component.html',
  styleUrls: ['./diet.component.css']
})
export class DietComponent implements OnInit {

  private days: Array<any>;
  constructor() { }

  ngOnInit() {
    this.days = ['monday','tuesday','wednesday','thursday', 'friday', 'saturday', 'sunday'];
     // this._profileService.obtainUserData() //See service file for comment
    //   .subscribe(response => {
    //     console.log(response);
    //     this.userData = response;
    //   });
    
    //  this._profileService.ObtainchartData()//See service file for comment
    //  .subscribe(response => {
    //    this.chartData = response;
    //  })

    //  this._profileService.ObtainStatusData()//See service file for comment
    //  .subscribe(response => {
    //    this.statusData = response;
    //  })
  }

}
