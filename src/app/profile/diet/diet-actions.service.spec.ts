import { TestBed, inject } from '@angular/core/testing';

import { DietActionsService } from './diet-actions.service';

describe('DietActionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DietActionsService]
    });
  });

  it('should be created', inject([DietActionsService], (service: DietActionsService) => {
    expect(service).toBeTruthy();
  }));
});
