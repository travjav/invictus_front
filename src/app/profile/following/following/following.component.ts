import { Component, OnInit } from '@angular/core';
import { NavigationBarComponent } from '../../../navigation-bar/navigation-bar.component';
import { FollowingService } from '../following.service';
import { Router } from '@angular/router'
@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
  styleUrls: ['./following.component.css'],
  providers: [FollowingService]
})
export class FollowingComponent implements OnInit {

  constructor (
    private following: FollowingService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  getFollowers() {
    this.router.navigate(['/myFollowers']);

  }

}
