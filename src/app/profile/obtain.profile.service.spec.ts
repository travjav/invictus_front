import { TestBed, inject } from '@angular/core/testing';

import { Obtain.ProfileService } from './obtain.profile.service';

describe('Obtain.ProfileService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Obtain.ProfileService]
    });
  });

  it('should be created', inject([Obtain.ProfileService], (service: Obtain.ProfileService) => {
    expect(service).toBeTruthy();
  }));
});
