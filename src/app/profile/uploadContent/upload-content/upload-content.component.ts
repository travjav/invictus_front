import { NavigationBarComponent } from '../../../navigation-bar/navigation-bar.component';
// import { UploadContentService } from '../upload-content.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import {
  HttpClient, HttpResponse, HttpRequest,
  HttpEventType, HttpErrorResponse, HttpHeaders
} from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { of } from 'rxjs/observable/of';
import { catchError, last, map, tap } from 'rxjs/operators';
import { UtilitiesService } from '../../../Utilities/utilities.service'
import { UploadContentService } from '../upload-content.service'
import { FormBuilder, FormControl, FormGroup, Validators, FormsModule } from '@angular/forms';

@Component({
  selector: 'app-upload-content',
  templateUrl: './upload-content.component.html',
  styleUrls: ['./upload-content.component.css'],
  providers: [UtilitiesService, UploadContentService],
  animations: [
    trigger('fadeInOut', [
      state('in', style({ opacity: 100 })),
      transition('* => void', [
        animate(300, style({ opacity: 0 }))
      ])
    ])
  ]
})
//TODO put form and description in same http request
export class UploadContentComponent implements OnInit {
  private FileInformation: FormGroup;
  private comment: string;
  private headers: any;
  private fileType: any;
  /** Link text */
  @Input() text = 'Upload';
  /** Name used in form which will be sent in HTTP request. */
  @Input() param = 'file';
  /** Target URL for file uploading. */
  // @Input() target = 'http://invictusconnected.com:5432/addUserVideos';
  /** File extension that accepted, same as 'accept' of <input type="file" />. 
      By the default, it's set to 'image/*'. */
  @Input() accept = 'undefined'
  /** Allow you to add handler after its completion. Bubble up response text from remote. */
  @Output() complete = new EventEmitter<string>();
  private files: Array<FileUploadModel> = [];

  constructor(
    private fileUploadService: UploadContentService,
    private utilities: UtilitiesService,
    private _http: HttpClient
    // private _uploadContent: UploadContentService) { }
  ) { }
  ngOnInit() {
    this.FileInformation = new FormGroup({
      file_comment: new FormControl('', {
        validators: Validators.required
      })
    });
  }

  private onClick() {
    const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files.push({
          data: file, state: 'in',
          inProgress: false, progress: 0, canRetry: false, canCancel: true
        });
      }
      this.uploadFiles();
    };
    fileUpload.click();
  }

  cancelFile(file: FileUploadModel) {
    file.sub.unsubscribe();
    this.removeFileFromArray(file);
  }

  retryFile(file: FileUploadModel) {
    this.uploadFile(file);
    file.canRetry = false;
  }

  private uploadFile(file: FileUploadModel) {
    let fileDestinationType = null;
    let destination_identifier = null;
    const fd = new FormData();
    let extension = file.data.name.split(".").pop();
    if (extension == "png" || extension == "jpeg" || extension == "jpg" || extension == "tiff") {
      fileDestinationType = "http://invictusconnected.com:5432/addUserPhotos";
      destination_identifier = "photos";
    } else if (extension == "mp4" || extension == "avi" || extension == "flv" || extension == "wmv") {
      fileDestinationType = "http://invictusconnected.com:5432/addUserVideos"
      destination_identifier = "videos";
    } else if (extension == "mp3" || extension == "wma" || extension == "wav") {
      fileDestinationType = "http://invictusconnected.com:5432/addUserMusic"
      destination_identifier = "music";
    }
   console.log('***************'+destination_identifier)
    fd.append(destination_identifier, file.data)
    fd.append('comment', this.FileInformation.value.file_comment);
    fd.append('date', this.utilities.get_local_time());

    const req = new HttpRequest('POST', fileDestinationType, fd, {
      reportProgress: true
    });

    file.inProgress = true;
    file.sub = this._http.request(req).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      tap(message => { }),
      last(),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        file.canRetry = true;
        return of(`${file.data.name} upload failed.`);
      })
    ).subscribe(
      (event: any) => {
        if (typeof (event) === 'object') {
          this.removeFileFromArray(file);
          this.complete.emit(event.body);
        }
      }
    );
  }

  private uploadFiles() {
    const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    fileUpload.value = '';
    this.files.forEach(file => {
      this.uploadFile(file);
      // this._uploadContent.uploadMedia(file)
    });
  }

  private removeFileFromArray(file: FileUploadModel) {
    const index = this.files.indexOf(file);
    if (index > -1) {
      this.files.splice(index, 1);
    }
  }

  private file_to_upload() {
    this.fileType
  }
}

export class FileUploadModel {
  data: File;
  state: string;
  inProgress: boolean;
  progress: number;
  canRetry: boolean;
  canCancel: boolean;
  sub?: Subscription;
}