import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpResponse, HttpHeaders, HttpEventType } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, URLSearchParams } from '@angular/http';

@Injectable()
export class UploadContentService {

  constructor(
    private http: HttpClient) { }


        // const req = new HttpRequest('POST', fileDestinationType, fd, {
    //   reportProgress: true
    // });
  uploadMedia(date,fileDestinationType, fd): Observable<any> {

    return this.http.post('http://invictusconnected.com:5432/addUserPhotos',
      fd, {
        reportProgress: true,
        observe: 'events'
      }
    );
  }
}

