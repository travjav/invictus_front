import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavigationBarComponent } from '../../../navigation-bar/navigation-bar.component';
import { SettingsPermissionsService } from '../settings-permissions.service';
import { FormBuilder, FormGroup, Validators, FormsModule, FormArray } from '@angular/forms';
import { Router } from '@angular/router'
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
  providers: [SettingsPermissionsService]
})

export class SettingsComponent implements OnInit {
  private userSettings: Array<boolean>;
  private changePassword: FormGroup;
  private updatePrefrences: FormGroup;
  private subscribe: Subscription;
  private currentSettings;
  private userCheckBoxes: Array<any>;

  constructor(
    private fb: FormBuilder,
    private settingService: SettingsPermissionsService,
    private formBuilder: FormBuilder,
    private updateSettingsForm: FormBuilder) {
    this.changePassword = this.formBuilder.group({
      currentpassword: ['', Validators.required],
      newpassword: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
    this.userCheckBoxes = [
    ];
  }

  ngOnDestroy(): void {
    this.subscribe.unsubscribe();
  }

  ngOnInit(): void {
    this.settingService.getSettings()
      .subscribe(settingsData => {
        console.log(settingsData);
        this.userCheckBoxes = settingsData;
      })
  }

  updatePassword() {
    let originalPassword = this.changePassword.value.currentpassword;
    let newpassword = this.changePassword.value.newpassword;
    let confirmPassword = this.changePassword.value.confirmPassword;
    let email = '';
    if (newpassword != confirmPassword || !newpassword || !confirmPassword)
      alert('Please Check To Make Sure Your Passwords Match');
    this.settingService.changePassword(email, originalPassword, newpassword)
      .subscribe(res => {
        console.log(res);
      })
  }

  updateSettings() {
    let settingsArray =
      [{
        online: this.userCheckBoxes[0],
        diet: this.userCheckBoxes[1],
        routine: this.userCheckBoxes[2],
        sleep: this.userCheckBoxes[4],
        findMe: this.userCheckBoxes[5],
        email: this.userCheckBoxes[6],
        stack: this.userCheckBoxes[3]
      }];

    console.log(settingsArray)
    this.settingService.updateSettings(settingsArray)
      .subscribe(response => {
        console.log(response);
      })
  }
}
