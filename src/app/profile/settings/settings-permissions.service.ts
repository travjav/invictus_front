import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SettingsPermissionsService {

  constructor(
    private http: HttpClient
  ) { }

  changePassword(email, originalpassword, newPassword): Observable <any> {
    const url = "http://invictusconnected.com:5432/changePassword";
    return this.http.put(url, { email: email, password: originalpassword, newPassword: newPassword })
  }

  updateSettings(settingsArray): Observable <any> {
    const url = "http://invictusconnected.com:5432/updateSettings";
    return this.http.put(url, { 
      settings: settingsArray
     })
  }

  getSettings(): Observable <any>{
    const url = "http://invictusconnected.com:5432/profile";
    return this.http.get(url);
  }
}
