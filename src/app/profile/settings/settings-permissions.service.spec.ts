import { TestBed, inject } from '@angular/core/testing';

import { SettingsPermissionsService } from './settings-permissions.service';

describe('SettingsPermissionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SettingsPermissionsService]
    });
  });

  it('should be created', inject([SettingsPermissionsService], (service: SettingsPermissionsService) => {
    expect(service).toBeTruthy();
  }));
});
