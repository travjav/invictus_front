import { TestBed, inject } from '@angular/core/testing';

import { SetSupplementsService } from './set-supplements.service';

describe('SetSupplementsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SetSupplementsService]
    });
  });

  it('should be created', inject([SetSupplementsService], (service: SetSupplementsService) => {
    expect(service).toBeTruthy();
  }));
});
