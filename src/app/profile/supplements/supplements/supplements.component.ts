import { Component, OnInit } from '@angular/core';
import { NavigationBarComponent } from '../../../navigation-bar/navigation-bar.component';
import { GetSupplementsService } from './get-supplements.service';
import { SetSupplementsService } from './set-supplements.service';
import { StarsComponent } from '../../../rating/stars/stars.component';
//import { Subscription } from "rxjs/Subscription";
import { StarRatingModule } from 'angular-star-rating';
import { Router, RouterModule } from '@angular/router'
@Component({
  selector: 'app-supplements',
  templateUrl: './supplements.component.html',
  styleUrls: ['./supplements.component.css'],
  providers: [GetSupplementsService, SetSupplementsService],
})

//TODO fix stars rating system
export class SupplementsComponent implements OnInit {
  preworkoutName: any;
  preWorkoutReview: any;
  intraworkoutName: any;
  intraworkoutReview: any;
  postworkoutName: any;
  postworkoutReview: any;
  miscName: any;
  miscReview: any;
  supplementData: any;
  contenteditable: boolean = false;
  preWorkout: any;
  editFlagPreworkout: boolean;
  editFlagIntra: boolean;
  editFlagPost: boolean;
  editFlagVitamins: boolean;
  stack: Array<any>;
  text: string;
  usersStack: Object;

  constructor(
    private getSupplements: GetSupplementsService,
    private setSupplements: SetSupplementsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getSupplements.currentStack()
      .subscribe((results) => {
        this.supplementData = results;

        this.preworkoutName = this.supplementData.map(function (name) {
          return name.template[0].preworkout
        });

        this.preWorkoutReview = this.supplementData.map(function (review) {
          return review.template[0].preworkoutReview
        });

        this.intraworkoutName = this.supplementData.map(function (name) {
          return name.template[0].intraworkout
        });

        this.intraworkoutReview = this.supplementData.map(function (review) {
          return review.template[0].intraworkoutReview
        });

        this.postworkoutName = this.supplementData.map(function (name) {
          return name.template[0].postworkout
        });

        this.postworkoutReview = this.supplementData.map(function (review) {
          return review.template[0].postworkoutReview
        });

        this.miscName = this.supplementData.map(function (name) {
          return name.template[0].miscname
        });

        this.miscReview = this.supplementData.map(function (review) {
          return review.template[0].miscReview
        });

        console.log(JSON.stringify(this.supplementData))
      })

    this.editFlagPreworkout = false;
    this.editFlagIntra = false;
    this.editFlagPost = false;
    this.editFlagVitamins = false;
    // this.thisWeek();
    // this.currentStack();
    this.preWorkout = StarsComponent;
  }

  currentStack() {
  }

  updateStack(): void {
    this.router.navigate(['/editSupplements'])
  }


  toggleContenteditable() {
    this.contenteditable = !this.contenteditable;
  }
}
