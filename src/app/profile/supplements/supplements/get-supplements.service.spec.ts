import { TestBed, inject } from '@angular/core/testing';

import { GetSupplementsService } from './get-supplements.service';

describe('GetSupplementsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetSupplementsService]
    });
  });

  it('should be created', inject([GetSupplementsService], (service: GetSupplementsService) => {
    expect(service).toBeTruthy();
  }));
});
