import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable()
export class PlaylistTipsActionsService {
  userData: any;
  constructor(private http: HttpClient) { }

  obtainUserData(userID): Observable<any> {
    return this.http.get('http://invictusconnected.com:5432/getUsersSongs', {
      params: {
        user: userID
      }
    })
  }
}
