import { Component, OnInit } from '@angular/core';
import { NavigationBarComponent } from '../../../navigation-bar/navigation-bar.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PlaylistTipsActionsService } from '../playlist-tips-actions.service'

@Component({
  selector: 'app-playlist-tips',
  templateUrl: './playlist-tips.component.html',
  styleUrls: ['./playlist-tips.component.css'],
  providers: [PlaylistTipsActionsService]
})
export class PlaylistTipsComponent implements OnInit {
  song: any;

  songList: any;
  userID: any;

  constructor(
    private playlistService: PlaylistTipsActionsService
  ) { }

  ngOnInit() {
    this.userID = 117
    this.playlistService.obtainUserData(this.userID)
      .subscribe((res) => {
        this.song = res;
        this.songList = this.song(function (files) {
          return files.song;
        })
      })
  }
}
