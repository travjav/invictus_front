import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistTipsComponent } from './playlist-tips.component';

describe('PlaylistTipsComponent', () => {
  let component: PlaylistTipsComponent;
  let fixture: ComponentFixture<PlaylistTipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaylistTipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistTipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
