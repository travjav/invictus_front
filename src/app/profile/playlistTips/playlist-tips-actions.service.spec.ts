import { TestBed, inject } from '@angular/core/testing';

import { PlaylistTipsActionsService } from './playlist-tips-actions.service';

describe('PlaylistTipsActionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlaylistTipsActionsService]
    });
  });

  it('should be created', inject([PlaylistTipsActionsService], (service: PlaylistTipsActionsService) => {
    expect(service).toBeTruthy();
  }));
});
