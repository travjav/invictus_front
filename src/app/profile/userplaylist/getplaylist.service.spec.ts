import { TestBed } from '@angular/core/testing';

import { GetplaylistService } from './getplaylist.service';

describe('GetplaylistService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetplaylistService = TestBed.get(GetplaylistService);
    expect(service).toBeTruthy();
  });
});
