
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
@Injectable()
export class GalleryService{

  userData: string;

  constructor(
    private http: HttpClient
  ) { }

  obtainImages(): Observable <any> {
    console.log("sendinggg")
    return this.http.get('http://invictusconnected.com:5432/getUserPhotos')
    .catch( (error: any) => Observable.throw(error.json().error || 'server error') )
  }
}