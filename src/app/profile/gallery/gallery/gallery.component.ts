import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { Subscription } from 'rxjs/Subscription';
import { GalleryService } from '../gallery.service';
//https://www.npmjs.com/package/ngx-gallery

@Component({
    selector: 'app-gallery',
    templateUrl: './gallery.component.html',
    styleUrls: ['./gallery.component.css'],
    providers: [GalleryService]
})
export class GalleryComponent implements OnInit {
    private userImages: any;
    galleryOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[];
    private subscribe: Subscription;

    constructor(
    private galleryService: GalleryService
    ) { }

    ngOnInit() {
        console.log("mounted")
        // this.galleryService.obtainImages().subscribe((data: any) => {
        //     this.userImages = data;
        //     console.log(this.userImages)
        // })

        // this.galleryOptions = [
        //     {
        //         width: '600px',
        //         height: '400px',
        //         thumbnailsColumns: 4,
        //         imageAnimation: NgxGalleryAnimation.Slide
        //     },
        //     // max-width 800
        //     {
        //         breakpoint: 800,
        //         width: '100%',
        //         height: '600px',
        //         imagePercent: 80,
        //         thumbnailsPercent: 20,
        //         thumbnailsMargin: 20,
        //         thumbnailMargin: 20
        //     },
        //     // max-width 400
        //     {
        //         breakpoint: 400,
        //         preview: false
        //     }
        // ];




        // this.galleryImages = [
        //     {
        //         small: 'https://www.joeweider.com/wp-content/uploads/2012/04/scan0004.jpg',
        //         medium: 'https://img.wennermedia.com/article-leads-horizontal/elon-musk-rolling-stone-interview-ae137f40-5cf4-4b28-bc47-d37b06b60310.jpg',
        //         big: 'https://www.teslafan.cz/media/images/clanky/musk-umela-inteligence-prekona-cloveka-do-roku-2040.jpg'
        //     },
        //     {
        //         small: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLXcTHm6xrv5pjLKfiQZ2kTFsvliBuPSflxwPgw4kvpfothjz3Pw',
        //         medium: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT4yVMJuon10wovAwsDrNXrlA5QgPsV0nAHY_qlJ_hlq1wewJwe',
        //         big: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIQERUQEBAVFhUVFRgVFRUWFxYVGBgVFRcWFxUVFhUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy0lICItLS0tKy0tLS01KystLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS03LS4tLS0tLS0tLf/AABEIAKIBNgMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABgEDBAUHAgj/xABEEAABAwIEAwUEBgYJBQEAAAABAAIRAwQFEiExBkFREyJhcYEHMpGhFEKxwdHwIyRSYpLhFjM0U3KCorLxQ2ODs9IV/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAEDBAIFBv/EACkRAAICAQQCAgEEAwEAAAAAAAABAgMRBBIhMQVBIlETMpGhsWFxwRT/2gAMAwEAAhEDEQA/AOIrLwuv2dVrvGD6rERXVT2SUl6IayjqWGDNBC2tduX4KO8FXXaUtd2mFJrqoDEL6qM96Ul0zzZJqWDCDp3VA4tKulis1DorUsleCzjDG1qD2nePmuVvbBI6LqnmudY9bdnWcORMj1Xl+TqzBT+jXpn2jBpPyuDhyMrqWD3gqU2vB5fNcrUl4OxLI/snHR23msvjrlGex+yzUV7o5+jojKgV5jtQta0rJt36r3cGA3DKYmVHeMMN+k0y2O83VpW3ZUIWRAe3UaqhxXUuUdLMXk4RcUHMcWuEEK2uq8R8KNuWlzBDxsevmuZ39hUoPLKjSCPzovD1OmdTyujfXap/7LuE31WlUHYk5iQA0cyTAELtuDWlQ02mrlzxLo2B6ark3AVuHXjXuBLaQLyAJJMZWgDrLp9F2nCcRY8OHYvY5oktqDKSOR32WK3VWqH41LCPR0lEG98ll+jZjHblrcpc1wiILRt00UbxOmyo4uFPIT+zt/Cr1THBnLXNpNj6pcc0eJiAsplRp1jQiYKz1X21PMJGqymqaacSLOaQYKyrOnOqweOa7qFPtqP1SCQde6TB+cH4qD/01uAIGUei+m0/kqp1pzeH7XJ8/fo5QnhdHT3EASSAo9jHEdvR+tmd0C55e43Xre/Ud5Aws7AuGatyczpazmTufJS9fueKo5Zz+GMeZsyLviSvcHJRaRPIbrNw3gytWIfcPInluVL8Pwejasim0T1O5Wp4g4iNuwhp752Cs/BKcd98uF69EKbfEFgjnFtjbWwFKk2X8zzCiqu3Vw6q4veZJOpVpeHqrYzn8FhI1wjhYC6f7K8NLqL6nV0T5LmC657M73LZZRvmKu0Cbs4KNVu2fH7JXegBuWVralHWUuLjWZVpt1K+hhBpGLbhGSsa5fl1lVq3Gmig/GeLOZ+jaYJ38lMpKuLnLpFkIb3g2+IcZUaJIAzuHRRbFuNbit3Wfo2+G/xUYJndF4F/krJ/o4X8m2NMUe6lVzjLnEnxMrwqIvOcnJ5ZcVRURQCqIikEq4FvMrn0+okKbMrLmXDlfJcNPXRdDa5fTeNkp0JfRhvj8jMfUWKVRz15BW9JIpKkKKcZ23u1PQqXEytPxNa56DtNRqqNTVvqlH/BZU8SRz9eqby0hwOoXhF8om0zeT7AMebVAY8w/bXmpVTbAlcYa4gyFLOHuKywinX1btm5he7pPIRliNnD+zJZR7idDtWFxjqpFaWPd1VnAaNOowVKbg4HmFtmuDdJVl1254iePqL3nbE19a1ybKL49gVO6BbUGvJ3MKa1nAqwLVrlzGax8hTqdq+Rzzhbgt1GpWZVMscGFpBInKXEgwRpqFL7W7tLdha6tTblGQiTpGuu+vzXviW2c1jHUzGpYf8ANBH+2PVR/CKtvD7i4r29tlqvpB1VjnVHOYGS4AREgjroF85q4L8zS6PrvH6iM9NGf+/7JRaV7eoBUZU0cYDgSGu6eapiRY0SDt81pG4my4eG2l1QumHR7QypSe0Qe+WvkFvw35q9cnL3SZgf/UfLKscltPQjNTXBi3lmLthpvAh+ZrWl2XMQJMH8Ok8lzlvBVY1HNJDWhxAJ1JbOkxzhdMt8YptApODswh1M5ZGaXcwZBEfNYtRy9jxOk/NulPpf2eX5KxRSS7/4abB+ErejBd33dT+C3ho5fdgDwWLUrZdSdFFuIOLMoNOiZPNy+i21aeOekeMoSmzacRY+2g0iZfyH4rm97duquL3nUrxXrOeS5xklW14Os10rvjHiP9m6upQQREXnloXUOAobajxJK5eukcEVJtQByJXq+JSdrz9GfUfpNxc1plW2VeSt1dCrYcvo2Z9qwZ4doub8XVJuCOimV/jVOg3vHXkFzvEbrtajn9SvM8pao07M8stog08mMqKqovmjWERFACIiAKqoikFyjUyuDhyMrpdlWD2Nd1AXMVLeGcSBZ2ROo28l7PibkpOD9lF0crJJyV4zLwHKi94y4Mu3frqsm9s8zCI3CwLIZnhb6vUiB4LiTaeDmXD4OMYjbGlUcw8isZTvjLBu0HbUxqNx1CghC+Z1tDqsf0+jfXPcgiIsh2bfAuI7izM0ahA5tOoPouiYB7R6L9LkFruo2XJEWmrVTgsPlFNmnhZ2j6IteJbWqO5UafVZYvG7tcPRfN7KpGxI8lnUMbuGe7WcPVbI6yl9pr+TE/Gw9Hd8Ru8zCN+Y8xqtU7C6NeoKxt6NUwP6xoeIjmDof5LldLiy7BH6Qk8ucqYOx64taTHZJJa1zpluVxEuBEciTosXkJ1T2yrfPTPW8fD8UXB9dk8tqDaLYFNjBzyNDWgeACjmN3YDyWulzjJA8YAHwAChlTim5uHw98CfdHNbnDaTi4Og9ZPgvJmenGe55RszFNjC8gGZk8pC1eK8T0KWjTmPgrfGdnWrUAKLHOLHZnBu+UAiQN3akaBc4qNIJDgQQYIOhBG4IOxXs6DX/g0+yK5y2edralK3L+jbYrxDVr6TDegWnJRFTdqLLXmbKVFLoIiLOdBEVEBVTn2cXzRnpO33CgyuUK7qZzMJB6hadJeqbFJ9HFkN0cHWMbv6NJhc5wB6cyoHiPEr3yKYyjqtHWrueZc4k+KtLdqPLSl8auF9+zmFSj2XKtVzjLiSVbRF5MpOTzJ5LQiIuQEREAREQBVVEQFVfsbjs3h/Qqwisrm4SUl6DWSeWOK0qkQ6D4reUKbSJkLlAMbLIpX9Vu1R3xXu1+Xg184/sZ5U56Z1a1pd7RbKvTmPJcfbjtwNqpXv+kNz/fOXcvJ0N5Wf2Kv/ADy+zrf0QQuf8eYXTpFtRhAJ0LR9q0TseuTvWd8VhV7l9Qy9xcfErNqddVZW44bLK6ZReclpEReOaAiLcYBwvd339moFzQYLzDWA+L3QJ8BJQGnW6wDha5vdaTIZ/evlrJHIGCXHwaCVO8G9ldKmM2IXBJmezoEAR0c9zZM+AEdVN31GBop0mhjGgNaBsGjQNHQKcHSiRDA+FaFg7NHbVYOWo4AZSP7tmoafEyfJZ1Sx7YAEZiRrPjzK3otmuEO/48Vbt6wbII7wOyquXGUbNK0pYZD7vhTs3h9OPzrp8Fu7W2cGjOPULMvnkwQRprr9iW2Y6nXyH3LI+TeoJcmO5pY4ubOgkbb8hJ6mAsTGuFqF6AazQyoAB2tMAOMDd4iH8t9dNwt0MtQiGwG7nqenp9vlrdc0dYWumG2PJ5mompS4OSY/wFcWzTVpubXpDdzNHjrmpbwNNRI1UTX0S1tL3XPMc4H81rMT4Uw25Zk7NtPUkPpsbTfJ3JeB3vJ0hW4M7RwlF0HiD2Y1KYL7Kp2wH/TcAKkfukaP8tD5rny4aICIiAIiIAqIiAIiIAiIoAREQBERAEREAVVRFIKoiIAiIpyAiImQERe6FE1HNY0S5zg1o8XGB8yhBKOAuEvp9Q1KpLbemf0jhoXu37Jh6xueQPiF2QvysbTpgMY0BrWNEBrRsAFq8MpstaFO3pnusbHm7dzj4kyVnNqggHwXeCyKLRpSdTKuspEaBe2r0h0UyLxUogkO2cBoeo6FXJVAgMO6tm1CJDmwQSG8+sHzhVpuaZYzaY3kmN80GPCBvrryXnGWVDSd2U5gCdDB2MQeswdwo3wBaV6LXiqwtBcfeBBEQNJ6wVV+KKlkulqJuG1k0pUw0QvFRvgvWbkvJKuM5YfHRWSrz1YqFQSG1HNMgqB+0nh5hb/+hbtAkj6QwaAF21YDxJg+JB5lS+rcwDryKwKtdr5pu1bUaWPH7rhlP2o+Tlo44i9VaZY4tdu0lp8wYK8qs5CIt/wTw/8AT7kU3GKbBnqkb5R9UeJOnlJ5KG8HUYuTwiPopDx3Y21veOpWgIY1rcwLi6HmZAJ1iMp16lR5E8iUXFtMIiKTkIiKAEREAREQBERAERFIKot/wNibbW8ZVeBEObJ+qXDQjp09VsvaJh9PtBd0Gw2oSKuX3RU3Dh/iE+oPVc7ucFqrbhvXohyIi6KgiIpAW44RpzeUj+yS4/5QdhuTMaDVadTv2ZYQXmpcGo0DWm1sS4u0JOb6o15GT6IuwiWue4nunTeXOYz5Zp+S2NhW0AzAweR016H4rDqWrgdKoB/fpsePQkB3xP3Tr696+lUYKhHeOWWjK2Tq0jz+9WHRMGar0Qtfh93O62AqSh0UIQDVCVQOUEntwheIVXuVvMhyyuZCZXgv2V0OCAsVmlY9eplHosi4rgbFR3F73uuI5D5oDxf3BLgzfNqRMaDU6/D4qxnp6naD7re/EcydgfMrU0Lntbh06gMAA6zrrG/lzW5ZAHeIaOTWhs+hjT0+Kgk5jj9LJc1h/wBxxG3uuOZp08CFgKT8d2jRW7em2oA6G1C8ye0AjnqAWgaHmDyhRhcHDWAuncA4HUo2VS6cNawHZt2IY2QHepcfQA81AMAsPpFzRoHZ9Rod/hmXf6QV9C4jc0qFKXd2mxusDZo2EchsFXN+jXo4fLe/Rxu5wvsqVWne25F3XfNOoXyGCGuAGUkSSSDOoAUPIhS/iOuA6ocznMdL6Lju075Z5gEx5KHqYFeoSTSQREXZnCIiAIiKAEREAREQBbfhltqarheteafZmMhykPkQ4+AErULMwe87CvTrCJY4OEiRI2kIzqLSayb13At2936s1tWk7VtQPY0ZTtmDiCD5T4SupYFw4KduKNYipNMMeHagyO9ofHY7rU8JcQMrl73EMe45nawyY0LfE/brzUrbUMTv+dlRKT6Z6VVcI5cfZwXijBzZXVS3kloMsJ3LHatnx5HxBWqXTvaBw3c3tVlW3o5y1ha8Ata6AZbDXEZtztK57fYVcUP6+3q0/F7HNHoSIKui8o8+2G2TRhottYcM3txHY2ddwOxFNwb/ABER81Jbb2TYi5oc/sKX7r6hLh55GuHzXWTlRb6REcGw511WZRZpmOp6N5ldzwexpUaLaLGDI0RHjzJ8SZMqNYBws2wL2lzalSATUAMCCCGtB1iVuaF7Ahx73NZbZ5fB6Gnq2xy+2XcQoQJa6R0dy8nfiudcWX2Qtylwc17XZDq3umRHIagbKUYxifJp1XPeIcxhzuZKsrsl0yq+uK5R0XDLsPY2o2YLQ4TpodR8lu7a7laW3aMrco0gR5RothRpzutZlNj9J/Z1HX8F6FVWaVIAICJUEmQHfmV5qCEBVyoEBjvqED7l5ZXzKocAfPRYFQmnUjl9xQgtYhUMkHktBiFfTXbMB8QVv8Sp6yB5eSjXENQ06Do31OvSND81DCNRgl2PpNRuaBIE76N7phTUMphpy7/tbu+PL0XKMNq5KjSTzg+qnFviXdG87RzJ+9ZbGzbptuOVyW74CtmpOEyIdA0B3DgdhyhQO5oGm9zHbtMfgfXddFw0Fxc76x2byiYk+Gm/PltrILrA7Kq1hr2ralRogvL6rCecQx4EBTGf2c2afK+Jz72cYdUrX1J7R3aZLnOOw7roHmum43xBSpdrbls1BTMMf3WuLmmNToWlQV+I/QLx4ps7Ok6mG04nKSMpBPjJeJMnvLDq4hUxCsGkucYOVrBmMAFzso5wAT6Lpx3ckVyVcXH3kj9/fFzRSB7oMkdD0WAszF6rH1nOpgAHLoNswaM5B5jNOqw1YZJSbeWURVVEOQiIgCIiAIiKAEREAREUgzbPEX04AOgMt8HdfkulcJ8YuqOyXL2FuUS+A0yByA3JOp22XKF7pvLTIMFcuKfZdXdKB3StxHSquyNIEajrGyqzG3td+jeR6/auK22Iua4OJ9VJbHHZ56qqVUl+k0xvjLs6qOIKj9HH1Vt2JPP1ioTZ440e8YWyZijXfWVbcvZatvo3vdJzu8z6bbLW43SpHvNMO5n7irNXEmxutHcXLnP7xBH53XCTLG0WK9CGl5PkoljN6H90cpW04lxqf0VM+ccv5qLrVXH2zBfZzhHYOHznt6T+tNpPnlE/OVuaTNFoOA6uaypeAc30D3AfcpMzeFqXRSUyd2VRjQOa9O0avAHNQyS8PD8+iujUKy3byV1h0UAxa7PErHxCnIDll3AlY1zUlnrqhBi13d1pO0wfVRbjNw7B0chHhuFIbpssI6KK8Tv/AFZ/p/uCgeiDgyuh4PlgkgRDZMbzyXOVKsJxAuY2D7sBw8uaosjkv008PDJ1Qa33gIVL29DRutE7G2xEhanEMVLtAVxGuUma5Wxij1xFfNqDJvHyPh4qLVLhzD3TEfeIP2q7d3cmB8VgrQlt4R59s97yERFBUEREBRFVEBRERAERFACIiAIiKQVREQBVa4jUGFREBl0sQc3x+SzKeMkdR81qEU5z2dKbXRt3Y47qfksR+JVDMOifU/FYaKA5yfsFERDk6f7Oq36q392o5p9Yd96mz6WoI5KAezX+z1B1rH4hjF0K1dLVaujpFqo3WVbOiv1yB4rHzaISe2FXKZCtNcqOcQZUA91mrBqN0cOULYtAcJ5rD2dqgNUTLSFEeLdKDvFzR8wfuU1rANqFvXUadf5z8VC/aGQG0wN3PJ/hbv8A6lD6DfBCF6pVC0y0kFeUXBwZYvnTLgD4jReK92XaDRY6Lrc8YJywiIuSAiIgCIiAIiIAqKqICiIqoCiIigBERSAiIgKoqKqAIiIAiIgCIiA6d7M6f6lUd0rk/BjFMTcAN7u5+Si/suEWbpGjqriPQNGnwUkt3+8NN+fRXLolFG/8lXPtVwQeUfL1XrsdJ3UEmI8wsimc7TqvFSnPJWKDsjgCfPxUEly1rQ7Iee3mvF7oZjZer2nl7wXvMHtnpuNUINZiuobUA0Gh9VDOPLZ9SlTrNEtpuc156dpkymOktInyU5bVa4FmgBWmrYdna+k5x7Oo0sdInKd2u8crg0+iNA5Qiv3tq6jUdSqCHMJafTmOoO4PQqwqzkIiIAiIgCIiAIiIAiIgCIiAIiICiIigBFVEBQKqIpAREQFFVEQBUVUQBERSDsXs2H6jS/8AL/7Ct2QO0fpy+5EVq6JL4Onp+CuO9wIiEnl/u/nwWpv/AMERckmwpGabZ/O6sWujnAdD9qqiIg1uKtAeIELFrV3tLcr3CTrBInzhEUg0HtipNbcW5a0CbcTAAnvHfqoAVVFW+zllERFACIiAIiIAiIgCIiAIiIAiIgP/2Q=='
        //     },
        //     {
        //         small: 'assets/3-small.jpg',
        //         medium: 'assets/3-medium.jpg',
        //         big: 'assets/3-big.jpg'
        //     }
        // ];
    }


}


