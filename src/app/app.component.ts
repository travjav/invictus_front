import { Component, OnInit, OnDestroy } from '@angular/core';
import { LandingPageComponent } from './landingPage/landing-page/landing-page.component';
//import { HomepageComponent } from './homepage/homepage.component';
import { Router, RouterModule } from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
   // this.router.navigate(['/landing']);
  }

}
