import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, URLSearchParams } from '@angular/http';
@Injectable()
export class HandleactionsService {

  constructor(
    private http: HttpClient) { }

  postContent(date, username, email, postText, convertedImage): Observable<any> {
    alert(convertedImage)
    return this.http.post('http://invictusconnected.com:5432/addPost', {
      "date":date, 
      "username": username,
      "email": email,
      "authorComment": postText,
      "image": convertedImage
    })
    .catch( (error: any) => Observable.throw(error.json().error || 'server error') )
  }

  goLive() {

  }

  speechPost() {

  }

  uploadVideo() {

  }
  uploadPhoto() {

  }

}
