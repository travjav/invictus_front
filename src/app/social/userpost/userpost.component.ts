import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { HandleactionsService } from './handleactions.service';
import { FormBuilder, FormControl, FormGroup, Validators, FormsModule } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import * as RecordRTC from 'recordrtc';
import { UtilitiesService } from '../../Utilities/utilities.service'
import {
  HttpClient, HttpResponse, HttpRequest,
  HttpEventType, HttpErrorResponse, HttpHeaders
} from '@angular/common/http';
import { of } from 'rxjs/observable/of';
import { catchError, last, map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-userpost',
  templateUrl: './userpost.component.html',
  styleUrls: ['./userpost.component.css'],
  providers: [HandleactionsService, UtilitiesService]
})
// declare var MediaRecorder: any; // For MediaRecorder Object
export class UserpostComponent implements OnInit {
  private postForm: FormGroup;
  private avatar: string;
  private postImages: Array<string>;
  private posterUsername: Array<string>;
  private posterTitle: Array<string>;
  private posterReps: Number;
  private posterComment: Array<string>;
  private postUsernameResponse: Array<string>;
  private posterCommentUsername: Array<string>; // look into putting all these into a object or arrays
  private postDate;
  private subscribe: Subscription;
  private convertedImage: any;
  private files: Array<FileUploadModel> = [];
  private FileInformation: FormGroup;
  @Output() complete = new EventEmitter<string>();

  constructor(
    private _http: HttpClient,
    private utilities: UtilitiesService,
    private handleAction: HandleactionsService,
    private _fb: FormBuilder
  ) {

    this.avatar = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Elon_Musk_2015.jpg/440px-Elon_Musk_2015.jpg";
    this.postImages = [];
  }

  ngOnDestroy(): void { }

  ngOnInit() {
    this.FileInformation = new FormGroup({
      file_comment: new FormControl('', {
        validators: Validators.required
      }),
      voice_recording: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      }),
      media_upload: new FormControl('', {
        validators: Validators.required,
        updateOn: 'submit'
      }),
      post_text: new FormControl('', {
        validators: Validators.required
      }),
    });
  }

  private onClick(): void {
    const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files.push({
          data: file, state: 'in',
          inProgress: false, progress: 0, canRetry: false, canCancel: true
        });
      }
    };
  }

  private uploadFiles(): void {
    const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    fileUpload.value = '';
    this.files.forEach(file => {
      this.uploadFile(file);
    });
  }

  private uploadFile(file): void {
    const fd = new FormData();
    fd.append('userPost', file.data)
    fd.append('date', this.utilities.get_local_time());
    fd.append('username', this.utilities.get_user_name());
    fd.append('email', this.utilities.get_email());
    fd.append('postText', this.FileInformation.value.post_text);
    const req = new HttpRequest('POST', "http://invictusconnected.com:5432/addPost", fd, {
      reportProgress: true
    });
     this.files = []
    file.inProgress = true;
    file.sub = this._http.request(req).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      tap(message => { }),
      last(),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        file.canRetry = true;
        return of(`${file.data.name} upload failed.`);
      })
    ).subscribe(
      (event: any) => {
        if (typeof (event) === 'object') {
          this.removeFileFromArray(file);
          this.complete.emit(event.body);
        }
      }
    );
  }

  private removeFileFromArray(file) {
    const index = this.files.indexOf(file);
    if (index > -1) {
      this.files.splice(index, 1);
    }
  }



  post_voiceRecording() {

    // this.postForm.value.voice_recording;
  }

  media_upload() {
    this.postForm.value.media_upload;
  }


  speakAction(): void { //media Recorder API
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      let audioStream = new MediaStream();
    } else {
      console.log('getUserMedia not supported on your browser!');

    }
    this.handleAction.speechPost();
  }

  uploadMedia() {
    let upload;
    if (upload == 'photo')
      this.handleAction.uploadPhoto();

    else

      if (upload == 'video')
        this.handleAction.uploadVideo();
  }

  postContent(): void {

  }

}
export class FileUploadModel {
  data: File;
  state: string;
  inProgress: boolean;
  progress: number;
  canRetry: boolean;
  canCancel: boolean;
  sub?: Subscription;
}