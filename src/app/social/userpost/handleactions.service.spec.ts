import { TestBed, inject } from '@angular/core/testing';

import { HandleactionsService } from './handleactions.service';

describe('HandleactionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HandleactionsService]
    });
  });

  it('should be created', inject([HandleactionsService], (service: HandleactionsService) => {
    expect(service).toBeTruthy();
  }));
});
