import { Component, OnInit } from '@angular/core';
import { UserpostComponent } from '../userpost/userpost.component';
import { SocialService } from '../social.service';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map'
import { containsElement } from '@angular/animations/browser/src/render/shared';

@Component({
  selector: 'app-social-media',
  templateUrl: './social-media.component.html',
  styleUrls: ['./social-media.component.css'],
  providers: [SocialService]
})
export class SocialMediaComponent implements OnInit {
  private postAuthor: any;
  private postObjects: any;
  private postAvatar: any;
  private postTitles: any;
  private postImages: any;
  private postHeader: any;
  private postContent: any;
  private postDate: any;
  private postMedia: any;
  totalRows$: Observable<number>;
  //https://stackblitz.com/edit/table-like-mat-accordion?file=app%2Fapp.component.ts

  private commentSection: any; // for comments
  private postComments: any;
  private postLikes: any;
  private commentAuthors: any;
  private postDislikes: any;
  private postCommentDates: any;
  private postPayload: Array<any>;
  private convertedDate: any;

  constructor(
    private payloads: SocialService
  ) { }

  ngOnInit() {
    console.log("getting stuf")
    this.payloads.loadPosts()
      .subscribe((res) => {
        this.postObjects = res;
        console.log(this.postObjects)
        this.postAuthor = this.postObjects.map(function (author) {
          return author.author;
        })

        this.postAvatar = this.postObjects.map(function (image) {
          return image.avatar;
        })

        this.postTitles = this.postObjects.map(function (title) {
          return title.title;
        })

        this.postContent = this.postObjects.map(function (content) {
          return content.postContent;
        })

        this.postDate = this.postObjects.map(function (date) {
          return new Date(date.upload_date)

        })

        this.postMedia = this.postObjects.map(function (media) {
          return media.image;
        })


        this.commentSection = this.postObjects.comments; // for comment sections
        this.postComments = this.commentSection.map(function (comments) {
          return comments.postComments;
        })

        this.commentAuthors = this.commentSection.map(function (author) {
          return author.commentAuthor;
        })

        this.postLikes = this.commentSection.map(function (likes) {
          return likes.postLikes;
        })

        this.postDislikes = this.commentSection.map(function (dislikes) {
          return dislikes.dislikes;
        })

        this.postCommentDates - this.commentSection.map(function (newDate) {
          return new Date(newDate)
        })

      })


  }

}
