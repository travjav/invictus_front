//https://www.npmjs.com/package/ngx-cookie-service
import { Component, OnInit, NgModule } from '@angular/core';
import { StorageService } from './storage.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.css'],
  providers: [StorageService],
  
})

export class StorageComponent implements OnInit {

  constructor(
    private cookieService: CookieService,
    private storageService: StorageService) { }

  ngOnInit(): void {
  }

  makeRequest(): string {
    let tokenKey = this.storageService.getWebToken();
    return tokenKey;
  }

  setToken(value) {
    this.storageService.setWebToken(value);
  }
}
