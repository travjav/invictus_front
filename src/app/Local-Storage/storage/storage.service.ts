import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
@Injectable()
export class StorageService {
  cookieToken: string;

  constructor(
    public cookieService: CookieService) { }

  ngOnInit(){

  }

  getWebToken(): string {
    this.cookieToken = this.cookieService.get('sessionToken');
    return this.cookieToken;
  }

  setWebToken(value): void {
    console.log(value);
    this.cookieService.set('user_cookie', value);
  }
}


